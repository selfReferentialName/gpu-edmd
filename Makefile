# excessively simple hardcoded Makefile

cpu:
	cc -DCPU -x c edmd.cu -o edmd -lm -g

gpu:
	nvcc -DGPU edmd.cu -o edmd -lm -g -G

doc:
