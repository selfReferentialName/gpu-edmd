// simple thing to generate data

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

void f(double *in, double *out, int n)
{
	out[0] = sin(in[1]) + sin(in[n-1]);
	out[n-1] = sin(in[n-2]) + sin(in[0]);
	for (int i=2; i<n-1; i++) {
		out[i] = sin(in[i-1]) + sin(in[i+1]);
	}
}

void scale(double *x, double c, int n)
{
	for (int i=0; i<n; i++) {
		x[i] = c*x[i];
	}
}

void vecfsa(double *x, double c, double *y, double *out, int n)
{
	for (int i=0; i<n; i++) {
		out[i] = fma(x[i], c, y[i]);
	}
}

// runge-kutta order 4
void step(double *x, double h, int n, FILE *file, int write)
{
	double tmp[n], k1[n], k2[n], k3[n], k4[n];
	f(x, k1, n);
	scale(k1, h, n);
	vecfsa(k1, 0.5, x, tmp, n);
	f(tmp, k2, n);
	scale(k2, h, n);
	vecfsa(k2, 0.5, x, tmp, n);
	f(tmp, k3, n);
	scale(k3, h, n);
	vecfsa(k3, 1, x, tmp, n);
	f(tmp, k4, n);
	scale(k4, h, n);

	vecfsa(k1, 1/6, x, tmp, n);
	vecfsa(k2, 1/3, tmp, tmp, n);
	vecfsa(k3, 1/3, tmp, tmp, n);
	vecfsa(k4, 1/6, tmp, x, n);

	if (write) {
		float xs[n];
		for (int i=0; i<n; i++) {
			xs[i] = x[i];
		}
		fwrite(xs, sizeof(float), n, file);
	}
}

int main(int argc, char **argv)
{
	if (argc != 4) {
		fprintf(stderr, "Usage: data-gen file n m");
	}

	char *filename = argv[1];
	int n = atoi(argv[2]);
	int m = atoi(argv[3]);

	FILE *file = fopen(filename, "wb");

	double x[n];
	for (int i=0; i<n; i++) {
		x[i] = drand48();
	}

	for (int i=0; i<m; i++) {
		for (int j=0; j<7; j++) {
			step(x, 0.001, n, file, 0);
		}
		step(x, 0.001, n, file, 1);
	}

	fclose(file);
	return 0;
}
